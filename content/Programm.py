import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt


f,b,s = np.loadtxt('Messwerte.txt', unpack=True)

def pol(x):

	return np.sqrt (  ( (x**2-1)**2  )/ (9*(  (1-x**2)**2   +   9*x**2        ) )      )








y=b/4.35
plt.plot(f/160,y,'rx',label='Messung')
t = np.linspace(0, 300000,100000)
wo=1007.05
t=t/wo


plt.plot(t,pol(t), 'b-',label='Formel (5)')

plt.xscale('log')
plt.ylim(-0.0005,0.4)
plt.xlim(0.1,200 )
plt.ylabel(r'${{\frac{U_{Br}}{U_S}}}$')
plt.xlabel(r'${{\frac{f}{f_0}}}$')
plt.legend(loc='best')
 

plt.tight_layout()
plt.savefig('Messung1.pdf')
plt.show()
