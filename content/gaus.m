printf('Messung 1 \n')


t=[1000,330,670;500,497,503;332,598,402;1000,282,718;664,372,628;500,440,560];
s=0;
for i=1:1:6
	r2=t(i,1);
	r3=t(i,2);
	r4=t(i,3);
	g=sqrt(  (r3*0.002/r4)^2+ (r2*0.005/r4)^2 + (r2*r3*0.005/(r4^2))^2);
	w=r2*r3/r4;
	s=s+w;
	printf('%i. Gaus:  %f    %i. Wert:  %f',i,g,i,w )
	if i==3 || i==6
		printf(' Der Mittelwert beträgt: %f',s/3)
		s=0;
	end
	printf('\n')
		
end

printf('Messung 2 -  Reine Kap \n')
t=[992,397,603;750,330,670;450,230,770;750,642,358;450,518,482;399,490,510];
s=0;
for i=1:1:6
        c2=t(i,1);
        r3=t(i,2);
        r4=t(i,3);
        g=sqrt(  (r4*0.002/r3)^2+ (c2*10^(-9)*0.005/r3)^2 + (c2*10^(-9)*r4*0.005/(r3^2))^2);
       	w=c2*r4/r3;
	s=s+w;
	printf('%i. Gaus:  %f    %i. Wert:  %f',i,g,i,w )
	if i==3 || i==6
		printf(' Der Mittelwert beträgt: %f',s/3)
		s=0;
	end
	printf('\n')
end


printf('Messung 3 -  RC Kom \n')
t=[992,310,607;750,414,536;450,679,412];

for i=1:1:3
        c2=t(i,1);
        r2=t(i,2);
        r3=t(i,3);
	r4=1000-r3;
	gr=sqrt(  (r3*0.03/r4)^2+ (r2*0.005/r4)^2 + (r2*r3*0.005/(r4^2))^2);
        gc=sqrt(  (r4*0.002/r3)^2+ (c2*10^(-9)*0.005/r3)^2 + (c2*10^(-9)*r4*0.005/(r3^2))^2);
	wr=r2*r3/r4;
	wc=c2*r4/r3;
        printf('%i: R_x  %f || C_x %f || Wert R %f || Wert C %f \n',i,gr,gc,wr,wc )

end

printf('Messung 4 -  Indu \n')
t=[27.5,111,498;20.1,77,574;14.6,59,648];

for i=1:1:3
        l2=t(i,1);
        r2=t(i,2);
        r3=t(i,3);
        r4=1000-r3;
        gr=sqrt(  (r3*0.002/r4)^2+ (r2*0.005/r4)^2 + (r2*r3*0.005/(r4^2))^2);
        gl=sqrt(  (r3*0.002/r4)^2+ (l2*10^(-3)*0.005/r4)^2 + (l2*10^(-3)*r3*0.005/(r4^2))^2);
        wr=r2*r3/r4;
	wl=l2*r3/r4;
	printf('%i: R_x  %f || L_x %f || Wert R %f || Wert L %f \n',i,gr,gl,wr,wl )
end

printf('Messung 5 -  Max  \n')
t=[992,31,251;750,31,333;450,59,555  ];

for i=1:1:3
        c4=t(i,1);
        r2=1000;
        r3=t(i,2);
        r4=t(i,3);
        gr=sqrt(  (r3*0.002/r4)^2+ (r2*0.03/r4)^2 + (r2*r3*0.03/(r4^2))^2);
        gl=sqrt(  (r3*0.002*10^(-9)*c4)^2+ (r2*0.03*10^(-9)*c4)^2 + (r3*r2*0.002)^2);
	wl=r2*r3*c4*10^(-6);
	wr=r2*r3/r4;
        printf('%i. Gaus: R_x  %f || L_x %f || Wert R %f || Wert L %f \n',i,gr,gl,wr,wl )
end


